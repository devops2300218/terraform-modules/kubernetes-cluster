resource "ovh_cloud_project_kube" "this" {
  service_name = var.project_id
  name         = var.kubernetes_name
  region       = var.region
  version      = var.k8s_version

  private_network_id = var.network_id

  private_network_configuration {
    default_vrack_gateway              = format("%s%s", var.subnet_prefix, ".4")
    private_network_routing_as_default = true
  }

  timeouts {
    create = "1h"
    update = "1h"
    delete = "1h"
  }
}

resource "ovh_cloud_project_kube_nodepool" "this" {
  for_each = var.nodepools

  service_name   = var.project_id # required for unknown reason
  kube_id        = ovh_cloud_project_kube.this.id
  name           = each.key
  flavor_name    = each.value.instance_type
  desired_nodes  = each.value.desired_node_amount
  max_nodes      = each.value.maximum_node_amount
  min_nodes      = each.value.minimum_node_amount
  monthly_billed = each.value.monthly_billed
  autoscale      = each.value.autoscale

  timeouts {
    create = "1h"
    update = "1h"
    delete = "1h"
  }
}

resource "local_sensitive_file" "kube_config" {
  content  = ovh_cloud_project_kube.this.kubeconfig
  filename = "${path.root}/kubeconfig.yaml"
}
