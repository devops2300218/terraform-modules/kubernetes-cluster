output "k8s_fqdn" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].host
  sensitive = true
}

output "k8s_ca_cert" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].cluster_ca_certificate
  sensitive = true
}

output "k8s_client_cert" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].client_certificate
  sensitive = true
}

output "k8s_client_key" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].client_key
  sensitive = true
}

output "k8s_kube_id" {
  value       = ovh_cloud_project_kube.this.id
  description = "Kube ID"
}
