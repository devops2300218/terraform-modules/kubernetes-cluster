variable "region" {
  description = "Required - a valid OVHcloud public cloud region ID in which the kubernetes cluster will be available. Ex.: GRA9. Defaults to all public cloud regions. Changing this value recreates the resource."
  type        = string
}
variable "k8s_version" {
  description = "(Optional) kubernetes version to use. Changing this value updates the resource. Defaults to the latest available."
  type        = string
  default     = ""
}
variable "project_id" {
  description = "The cloud project id"
  type        = string
}

variable "subnet_prefix" {
  description = "The id of the OpenStack region"
  type        = string
}

variable "kubernetes_name" {
  description = "The name of the kubernetes cluster"
  type        = string
}

variable "network_id" {
  description = "The network_id the kubernetes cluster will be assigned to"
  type        = string
}

variable "nodepools" {
  description = "A map of nodepools which should be created in the kubernetes cluster"
  type = map(object({            # map key: "name for the kubernetes nodepool"
    instance_type       = string # "the compute instance type(flavor) which should be used as node"
    desired_node_amount = number # "desired amount of nodes in the nodepool"
    minimum_node_amount = number # "minimum amount of nodes in the nodepool"
    maximum_node_amount = number # "maximum amount of nodes in the nodepool"
    monthly_billed      = bool   # 50% cheaper, useful for non-scaling persistent pools
    autoscale           = bool
  }))
}
